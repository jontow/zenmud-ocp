################################################################################
#
# Clean it all up:
#
# $ oc delete all --all
#
# Build it all back again with template.yml:
#
# $ oc process -f template.yml | oc create -f -
#
# Build it all back again manually:
#
# $ oc new-build --to=mud -D - <Dockerfile
# $ oc logs -f build.build.openshift.io/mud-1
# $ oc new-app -i mud
# $ oc expose replicationcontroller/mud-1 --port=4000 --type=NodePort
#
################################################################################

FROM docker.io/centos:7
MAINTAINER Jonathan Towne <jontow@mototowne.com>

# These labels are used by OpenShift in order to display information inside the project
LABEL io.k8s.description="Just an old MUD codebase in a modern world" \
 io.k8s.display-name="ZenMUD" \
 io.openshift.expose-services="8080:tcp"

# Install common dependencies
RUN yum -y install epel-release wget unzip nmap-ncat && yum -y clean all

# Install and configure nginx
RUN yum -y install nginx && chgrp -R 0 /var/log/nginx && chmod -R g=u /var/log/nginx && chgrp -R 0 /var/lib/nginx && chmod -R g=u /var/lib/nginx && chgrp 0 /run && chmod g=u /run
COPY nginx.conf /etc/nginx/nginx.conf
COPY nginx_ws.conf /etc/nginx/conf.d/nginx_ws.conf

# Install and configure websocketd
RUN echo 1 && wget -O /opt/websocketd-0.3.1-linux_amd64.zip https://github.com/joewalnes/websocketd/releases/download/v0.3.1/websocketd-0.3.1-linux_amd64.zip && mkdir /opt/websocketd && unzip -d /opt/websocketd /opt/websocketd-0.3.1-linux_amd64.zip
COPY svc_conn.sh /opt/websocketd/svc_conn.sh

# Install and configure zenmud
RUN mkdir -p /opt/zenmud
COPY mudrun.sh /opt/zenmud/mudrun.sh
RUN echo 1 && wget -O /opt/zenmud-centos7-bin.tgz https://slite.zenbsd.net/~jontow/zenmud-centos7-bin.tgz && tar zxvf /opt/zenmud-centos7-bin.tgz -C /opt && chgrp -R 0 /opt && chmod -R g=u /opt

EXPOSE 8080

ENTRYPOINT ["/bin/sh", "/opt/zenmud/mudrun.sh"]
