#!/bin/sh

# Start websocketd
/opt/websocketd/websocketd --address=127.0.0.1 --port=8010 --binary=true /opt/websocketd/svc_conn.sh >>/opt/websocketd/websocketd.log &

# Start nginx
/usr/sbin/nginx

# Start MUD
cd /opt/zenmud/area || exit 1
exec ../rom -4 0.0.0.0 -p 4000
